-- Tạo cơ sở dữ liệu QLSV
CREATE DATABASE QLSV;

-- Sử dụng cơ sở dữ liệu QLSV
USE QLSV;

-- Tạo bảng DMKhoa
CREATE TABLE DMKhoa (
    MaKH VARCHAR(6) PRIMARY KEY,
    TenKhoa VARCHAR(30)
);

-- Tạo bảng SINHVIEN
CREATE TABLE SINHVIEN (
    MaSV VARCHAR(6) PRIMARY KEY,
    HoSV VARCHAR(30),
    TenSV VARCHAR(15),
    GioiTinh CHAR(1),
    NgaySinh DATETIME,
    NoiSinh VARCHAR(50),
    DiaChi VARCHAR(50),
    MaKH VARCHAR(6),
    HocBong INT,
    FOREIGN KEY (MaKH) REFERENCES DMKhoa(MaKH)
);


SELECT * FROM students
WHERE department = 'Công nghệ thông tin';
