<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Đăng nhập</title>
    <style>
        .border     {border: solid 2px steelblue;padding: 10px;margin: 5px;}
        .table      {display: grid;padding: 10px;grid-template-columns: 1fr 1fr;}
        .column1    {background: cornflowerblue;grid-column-start: 1;grid-column-end: 2;}
        .column2    {grid-column-start: 2;grid-column-end: 3;}
        .button     {border: solid 2px steelblue; border-radius: 6px; color:white; background: cornflowerblue;
                    padding-inline: 30px; padding-block: 15px;margin-top: 15px;}
    </style>
</head>
<body>
<h1>Đăng nhập</h1>
<div style="border: solid 2px steelblue; padding: 10vh;">
    <p style="background-color: lightgray; padding: 8px">
        <?php
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        echo "Bây giờ là: " . date('H:i, \t\hứ N \n\gà\y d/m/Y');
        ?>
    </p>
    <form action="login.php" method="post">
        <div class="table">
            <label class="column1 border" for="username" style="color: white">Tên đăng nhập</label>
            <input class="column2 border" type="text" name="username" id="username" placeholder="Tên đăng nhập">
            <label class="column1 border" for="password" style="color: white">Mật khẩu</label>
            <input class="column2 border" type="password" name="password" id="password" placeholder="Mật khẩu">
        </div>
        <div style="text-align: center;">
            <input class="button" type="submit" value="Đăng nhập">
        </div>
    </form>
</div>

</body>
</html>