<!DOCTYPE html>
<html lang="vi">
<head>
    <title>Register</title>
    <style>
        label {
            color: white;
            background-color: cornflowerblue;
            border: 2px solid royalblue;
            display: inline-flex;
            width: 100px;
            margin: 4px;
            padding: 4px;
            padding-left: 25px !important;
        }

        input:not([type='submit']), select{
            border: 2px solid royalblue;
            margin: 4px;
            padding: 4px;
        }

        input[type='submit'] {
            border: 2px solid royalblue;
            background: forestgreen;
            border-radius: 5px;
            margin-top: 30px;
            padding: 10px;
            color: white;
            font-size: 15px;
        }
    </style>
</head>
<body>
<div style="border: solid 2px steelblue; padding: 10vh; display: flex; justify-content: center; align-items: center;">
    <div>
    <form action="register.php" method="POST">
        <label for="username">Họ và tên</label>
        <input type="text" name="username"> <br>
        <label for="gender">Giới tính</label>
        <?php
        $gender = [
            0 => 'Nam',
            1 => 'Nữ',
        ];
        for ($i = 0; $i < 2; $i++) {
            echo '<input type="radio" name="gender" value="' . $i . '"> ' . $gender[$i];
        }
        ?> <br>
        <label>Phân khoa</label>
        <select name="facility" style="">
            <?php
            $faculty = [
                '' => '--Chọn phân khoa--',
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu',
            ];
            foreach ($faculty as $key => $value) {
                echo '<option value="' . $key . '"> ' . $value . '</option>';
            }
            ?>
        </select> <br>
        <div style="display:flex; justify-content: center;">
            <input type="submit" value="Đăng nhập">
        </div>
    </form>
    </div>
</div>
</body>
</html>

<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Get the form data
    $username = $_POST['username'];
    $genderKey = $_POST['gender'];
    $facultyKey = $_POST['facility'];

    // Display the form data
    echo 'Họ và tên: ' . $username . '<br>';
    echo 'Giới tính: ' . $gender[$genderKey] . '<br>';
    echo 'Phân khoa: ' . $faculty[$facultyKey] . '<br>';
}
?>