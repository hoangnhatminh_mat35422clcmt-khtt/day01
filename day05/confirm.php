<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Confirm</title>
    <style>
        label {
            color: white;
            background-color: forestgreen;
            border: 2px solid royalblue;
            display: inline-block;
            margin-block: 8px;
            padding-block: 4px;
            text-indent: 20px;
            width: 110px;
            margin-right: 20px;
        }

        input[type='submit'] {
            border: 2px solid royalblue;
            background: forestgreen;
            border-radius: 5px;
            margin-top: 10px;
            padding: 10px;
            color: white;
            font-size: 15px;
        }

    </style>
</head>
<body>
    <div style="border: solid 2px cornflowerblue; font-family: 'Times New Roman',serif; padding: 5%">
        <label>Họ và tên</label>
        <?php $username = $_POST['username']; echo "$username";?><br>
        <label>Giới tính</label>
        <?php
            $g = $_POST['gender'];
            $gender = [
                0 => 'Nam',
                1 => 'Nữ',
            ];
            echo $gender[$g];
        ?><br>
        <label>Phân khoa</label>
        <?php
            $f = $_POST['facility'];
            $facility = [
                '' => '--Chọn phân khoa--',
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu',
            ];
            echo $facility[$f];
        ?><br>
        <label>Ngày sinh</label>
        <?php $birthday = $_POST['birthday']; echo "$birthday";?><br>
        <label>Địa chỉ</label>
        <?php $address = $_POST['description']; echo "$address";?><br>
        <label>Hình ảnh</label>
        <?php
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $anh = $_FILES['image'];

                if ($anh['error'] != UPLOAD_ERR_OK) {
                    echo "Có lỗi khi tải lên file ảnh. Thử ảnh kích thước bé hơn.";
                    return;
                }

                if ($anh['size'] == 0) {
                    echo "Kích thước file ảnh bằng 0.";
                    return;
                }

                $target_dir = "images/";
                $target_file = $target_dir . basename($anh['name']);

                if (!move_uploaded_file($anh['tmp_name'], $target_file)) {
                    echo "Không thể di chuyển file ảnh đến thư mục đích.";
                    return;
                }

                echo "<img style='vertical-align: top; margin-top: 8px; margin-bottom: 15px' src='$target_file' alt='@@' width='200' height='200'>";
            }
        ?><br>
        <div style="display:flex; justify-content: center;">
            <input type="submit" value="Xác nhận">
        </div>
    </div>
</body>
</html>


