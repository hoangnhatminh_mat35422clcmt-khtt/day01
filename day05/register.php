<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Register</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <style>
        label {
            color: white;
            background-color: forestgreen;
            border: 2px solid royalblue;
            display: inline-block;
            margin-block: 8px;
            padding-block: 4px;
            text-indent: 20px;
            width: 110px;
        }

        .required::after{
            content: " *";
            color: red;
            font-size: 14px;
        }

        input[type="radio"] {
            color: forestgreen;
        }

        input[type='text'], input[type='date'], select{
            border: 2px solid royalblue;
            padding-block: 6px;
            margin-left: 15px;
        }

        input[type='submit'] {
            border: 2px solid royalblue;
            background: forestgreen;
            border-radius: 5px;
            margin-top: 20px;
            margin-bottom: 20px;
            padding: 10px;
            color: white;
            font-size: 15px;
        }

        input[type="date"] {
            color: #727272;
        }

        input[type="file"] {
            margin-left: 15px;
        }

        textarea {
            vertical-align: top;
            border: 2px solid royalblue;
            padding: 4px;
            display: inline-block;
            resize: none;
            padding-block: 6px;
            margin-block: 6px;
            margin-left: 15px;
        }
    </style>
</head>
<body>
    <div style="border: solid 2px cornflowerblue; font-family: 'Times New Roman',serif; padding-inline: 5%">

        <p id="message" style="color: red; margin-left: 2lh">

        </p>

        <form action="confirm.php" method="POST" enctype="multipart/form-data">
            <label class="required" for="username">Họ và tên</label>
            <input type="text" name="username" style="width: calc(100% - 150px)"> <br>
            <label class="required" for="gender">Giới tính</label>
            <?php
            $gender = [
                0 => 'Nam',
                1 => 'Nữ',
            ];
            for ($i = 0; $i < 2; $i++) {
                echo '<input style="margin-left: 15px" type="radio" name="gender" value="' . $i . '"> ' . $gender[$i];
            }
            ?> <br>
            <label class="required">Phân khoa</label>
            <select name="facility" style="width: 40%">
                <?php
                $facility = [
                    '' => '--Chọn phân khoa--',
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu',
                ];
                foreach ($facility as $key => $value) {
                    echo '<option value="' . $key . '"> ' . $value . '</option>';
                }
                ?>
            </select> <br>
            <label class="required">Ngày sinh</label>
            <input type="text" name="birthday" placeholder="dd/mm/yyyy" style="width: calc(40% - 5px)"><br>
            <label>Địa chỉ</label>
            <textarea name="description" rows="5" style="width: calc(100% - 150px)"></textarea>
            <label for="fileSelect">Hình ảnh</label>
            <input type="file" name="image" id="fileSelect"><br>
            <div style="display:flex; justify-content: center;">
                <input type="submit" value="Đăng nhập">
            </div>

        </form>
    </div>

    <script>
        $('input[type="submit"]').click(function() {

            let isQualified = 1;

            const message = $("#message");
            message.html("")

            const name = $('input[name="username"]').val();
            if (name === "") {
                message.append("Hãy nhập tên.<br>");
            } else {
                isQualified <<= 1;
            }

            const facility = $('select[name="facility"]').val();
            if (facility === "") {
                message.append("Hãy chọn phân khoa.<br>");
            } else {
                isQualified <<= 1;
            }

            const gender0 = $('input[name="gender"][value="0"]').is(':checked');
            const gender1 = $('input[name="gender"][value="1"]').is(':checked');
            if (!gender0 && !gender1) {
                message.append("Hãy chọn giới tính.<br>");
            } else {
                isQualified <<= 1;
            }

            const birthday = document.querySelector("input[name='birthday']");
            if (birthday.value === "") {
                message.append("Hãy nhập ngày sinh.<br>");
            } else {
                if (!birthday.value.match(/^\d{2}\/\d{2}\/\d{4}$/)) {
                    message.append("Hãy nhập ngày sinh đúng định dạng.<br>");
                } else {
                    isQualified <<= 1;
                }
            }

            if (isQualified === 16) {
                $(this).closest('form').submit();
            } else {
                return false;
            }
        });

    </script>
</body>


</html>
