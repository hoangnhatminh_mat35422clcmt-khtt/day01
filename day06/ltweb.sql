SET NAMES 'utf8';
SET CHARACTER SET utf8;
SET collation_connection = 'utf8_vietnamese_ci';

CREATE DATABASE IF NOT EXISTS ltweb;

USE ltweb;

CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL,
    gender ENUM('Nam', 'Nữ') NOT NULL,
    faculity VARCHAR(100) NOT NULL,
    date_of_birth DATE NOT NULL,
    address VARCHAR(255),
    image_path VARCHAR(255)
    );
