<?php
global$conn;
?>

<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Confirm</title>
    <style>
        .center {
            margin-left: 100px;
            width: 300px;
            padding: 10px;
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        label {
            display: flex;
            width: 100%;
            padding: 5px;
        }

        label span {
            width: 30%;
            padding: 5px
        }

        label input, label select {
            width: 70%;
            border: 2px solid royalblue;
            padding-block: 6px;
            background-color: rgba(166, 183, 210, 0.81);
        }

        .primary_button, input[type="submit"] {
            border: 2px solid steelblue;
            background: cornflowerblue;
            border-radius: 10px;
            margin: auto;
            padding-block: 8px;
            padding-inline: 25px;
            color: white;
        }

        td, th {
            text-align: left;
            padding-right: 30px;

        }

        th:nth-child(4), td:nth-child(4) {
            width: 30%;
        }

        th:nth-child(5), td:nth-child(5) {
            padding-right: 0;
        }

        table {
            width: 100%;
        }

        .second_bt {
            background-color: #7493ad;
            border: 2px solid steelblue;
            padding-inline: 10px;
            padding-block: 3px;
            margin-block: 5px;
            color: white;
        }
    </style>
    <script>
        function updateTotal(value) {
            document.getElementById('total').textContent = value;
        }

        function redirectToRegister() {
            window.location.href = "register.php";
        }
    </script>
</head>
<body>

    <form class="center" action="students.php" method="GET">
        <label>
            <span>Khoa</span>
            <select style="width: 75%" name="facility">
                <?php
                $facility = [
                    '' => '',
                    'MAT' => 'Khoa học máy tính',
                    'KDL' => 'Khoa học vật liệu',
                ];
                foreach ($facility as $key => $value) {
                    echo '<option value="' . $key . '"> ' . $value . '</option>';
                }
                ?>
            </select>
        </label>
        <label>
            <span>Từ khóa</span>
            <input type="text" name="keyword">
        </label>
        <label>
            <span></span>
            <input style="width: 40%" type="submit" value="Tìm kiếm">
        </label>
    </form>

    Số sinh viên tìm thấy: <span id="total">0</span> <br>
    <input style='float: right' class='primary_button' type='button' value='Thêm' onclick="redirectToRegister()"> <br><br>
</body>
</html>

<?php
include 'db_connect.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $conn = $_SESSION['db_connection'];

    $f = $_GET['facility'] ?? "";
    $k = $_GET['keyword'] ?? "";

    $sql = "SELECT * FROM students WHERE faculity LIKE ? AND full_name LIKE ?";
    $stmt = $conn->prepare($sql);
    $searchKeyword = '%' . $k . '%';
    $stmt->bind_param("ss", $f, $searchKeyword);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        echo "<table><tr><th>No</th><th>Tên sinh viên</th><th>Khoa</th><th></th><th>Action</th></tr>";
        echo "<script>updateTotal($result->num_rows)</script>";
        while ($row = $result->fetch_assoc()) {
            echo "<tr><td>" . $row["id"] . "</td><td>" . $row["full_name"] .
                "</td><td>" . $facility[$row["faculity"]] . "</td><td>" .
                "</td><td>" .
                "<input class='second_bt' type='button' value='Xóa'> <input class='second_bt' type='button' value='Sửa'></td></tr>";
        }
        echo "</table>";
    }

}
?>
