<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "ltweb";

// Tạo kết nối
$conn = new mysqli($servername, $username, $password, $database);

// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Kết nối đến cơ sở dữ liệu thất bại: " . $conn->connect_error);
}

// Xử lý lưu dữ liệu vào cơ sở dữ liệu
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $full_name = $_POST['username'];
    $gender = $_POST['gender'];
    $department = $_POST['facility'];
    $date_of_birth = $_POST['birthday'];
    $address = $_POST['address'];
    $image_path = $_POST['image_path'] ?? NULL;

    $full_name_escaped = addslashes($full_name);
    $gender_escaped = addslashes($gender);
    $department_escaped = addslashes($department);
    $date_of_birth_escaped = addslashes($date_of_birth);
    $address_escaped = addslashes($address);
    $image_path_escaped = addslashes($image_path);

    $sql = "INSERT INTO students (full_name, gender, faculity, date_of_birth, address, image_path) VALUES ('$full_name_escaped', '$gender_escaped', '$department_escaped', '$date_of_birth_escaped', '$address_escaped', '$image_path_escaped')";

    if ($conn->query($sql) === TRUE) {
        echo "Dữ liệu đã được lưu thành công vào cơ sở dữ liệu ltweb";
    } else {
        echo "Lỗi: " . $sql . "<br>" . $conn->error;
    }
}

$conn->close();
?>
