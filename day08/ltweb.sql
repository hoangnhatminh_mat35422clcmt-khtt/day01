SET NAMES 'utf8';
SET CHARACTER SET utf8;
SET collation_connection = 'utf8_vietnamese_ci';

DROP DATABASE IF EXISTS ltweb;

CREATE DATABASE IF NOT EXISTS ltweb;

USE ltweb;

CREATE TABLE IF NOT EXISTS students (
    id INT AUTO_INCREMENT PRIMARY KEY,
    full_name VARCHAR(100) NOT NULL,
    gender ENUM('Nam', 'Nữ') NOT NULL,
    faculity VARCHAR(100) NOT NULL,
    date_of_birth DATE NOT NULL,
    address VARCHAR(255),
    image_path VARCHAR(255)
    );

ALTER TABLE students ADD FULLTEXT(full_name, address);

INSERT INTO students (full_name, gender, faculity, date_of_birth, address) VALUES
('Nguyễn Văn A', 'Nam', 'Khoa học máy tính', '1990-05-15', '123 Đường A, Thành phố'),
('Trần Thị B', 'Nữ', 'Khoa học vật liệu', '1992-08-20', '456 Đường B, Thị xã'),
('Lê Văn C', 'Nam', 'Khoa học máy tính', '1991-03-10', '789 Đường C, Xã'),
('Phạm Thị D', 'Nữ', 'Khoa học vật liệu', '1995-12-05', '101 Đường D, Quận'),
('Nguyễn Văn E', 'Nam', 'Khoa học máy tính', '1993-07-25', '123 Đường E, Thành phố'),
('Trần Thị F', 'Nữ', 'Khoa học vật liệu', '1994-10-30', '456 Đường F, Thị xã'),
('Lê Văn G', 'Nam', 'Khoa học máy tính', '1996-01-18', '789 Đường G, Xã'),
('Phạm Thị H', 'Nữ', 'Khoa học vật liệu', '1998-04-12', '101 Đường H, Quận'),
('Hoàng Văn I', 'Nam', 'Khoa học máy tính', '1997-06-08', '123 Đường I, Thành phố'),
('Vũ Thị K', 'Nữ', 'Khoa học vật liệu', '1999-09-17', '456 Đường K, Thị xã'),
('Nguyễn Văn L', 'Nam', 'Khoa học máy tính', '1990-11-22', '789 Đường L, Xã'),
('Trần Thị M', 'Nữ', 'Khoa học vật liệu', '1992-02-28', '101 Đường M, Quận'),
('Lê Văn N', 'Nam', 'Khoa học máy tính', '1991-08-03', '123 Đường N, Thành phố'),
('Phạm Thị P', 'Nữ', 'Khoa học vật liệu', '1995-12-07', '456 Đường P, Thị xã'),
('Hoàng Văn Q', 'Nam', 'Khoa học máy tính', '1994-04-19', '789 Đường Q, Xã'),
('Vũ Thị R', 'Nữ', 'Khoa học vật liệu', '1998-01-28', '101 Đường R, Quận'),
('Nguyễn Văn S', 'Nam', 'Khoa học máy tính', '1997-05-03', '123 Đường S, Thành phố'),
('Trần Thị T', 'Nữ', 'Khoa học vật liệu', '1999-09-11', '456 Đường T, Thị xã'),
('Lê Văn U', 'Nam', 'Khoa học máy tính', '1990-02-16', '789 Đường U, Xã'),
('Phạm Thị V', 'Nữ', 'Khoa học vật liệu', '1993-06-21', '101 Đường V, Quận'),
('Hoàng Văn X', 'Nam', 'Khoa học máy tính', '1992-09-27', '123 Đường X, Thành phố'),
('Vũ Thị Y', 'Nữ', 'Khoa học vật liệu', '1991-11-04', '456 Đường Y, Thị xã'),
('Nguyễn Văn Z', 'Nam', 'Khoa học máy tính', '1996-03-14', '789 Đường Z, Xã'),
('Trần Thị W', 'Nữ', 'Khoa học vật liệu', '1995-07-02', '101 Đường W, Quận'),
('Hoàng Nhật Minh', 'Nam', 'Khoa học máy tính', '2002-07-08', 'Liên Trung, Đan Phượng, Hà Nội');