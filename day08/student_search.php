<?php
include 'db_connect.php';
$facility = [
    '' => '',
    'MAT' => 'Khoa học máy tính',
    'KDL' => 'Khoa học vật liệu',
];

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $conn = $_SESSION['db_connection'];

    $f = $facility[$_GET['facility']] ?? "";
    $k = $_GET['keyword'] ?? "";

    $sql = "SELECT * FROM students WHERE faculity LIKE ? AND full_name LIKE ?";

    $stmt = $conn->prepare($sql);
    $searchKeyword = '%' . $k . '%';
    $stmt->bind_param("ss", $f, $searchKeyword);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
        echo "<table><tr><th>No</th><th>Tên sinh viên</th><th>Khoa</th><th></th><th>Action</th></tr>";
        echo "<script>updateTotal($result->num_rows)</script>";
        while ($row = $result->fetch_assoc()) {
            echo "<tr><td>" . $row["id"] . "</td><td>" . $row["full_name"] .
                "</td><td>" . $row["faculity"] . "</td><td>" .
                "</td><td>" .
                "<input class='second_bt' type='button' value='Xóa'> <input class='second_bt' type='button' value='Sửa'></td></tr>";
        }
        echo "</table>";
    }
}
?>