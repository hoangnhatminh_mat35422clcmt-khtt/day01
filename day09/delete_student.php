<?php
include 'db_connect.php';
$facility = [
    '' => '',
    'MAT' => 'Khoa học máy tính',
    'KDL' => 'Khoa học vật liệu',
];

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $id = $_GET['id'];
    $sql = "DELETE FROM students WHERE id = ?";
    $stmt = $conn->prepare($sql);
    $stmt->bind_param("i", $id);
    if ($stmt->execute()) {
        $result = "đã xóa " . $id . " khỏi danh sách sinh viên!";
    } else {
        $result = "xóa thất bại!";
    }
}
?>