<?php
// Kết nối đến database
include 'db_connect.php';

// Kiểm tra kết nối
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $studentId = $_GET['id'];

    $stmt = $conn->prepare("SELECT full_name, gender, faculity, date_of_birth, address FROM students WHERE id = ?");
    $stmt->bind_param("i", $studentId);
    $stmt->execute();
    $stmt->bind_result($full_name, $gender, $faculity, $date_of_birth, $address);
    $stmt->fetch();
    $stmt->close();

    $studentData = array(
        'id' => $studentId,
        'full_name' => $full_name,
        'gender' => $gender,
        'faculity' => $faculity,
        'date_of_birth' => $date_of_birth,
        'address' => $address
    );


    echo json_encode($studentData);
}
?>
