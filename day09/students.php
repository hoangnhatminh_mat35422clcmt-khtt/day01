<?php
$selectedFacility = isset($_GET['facility']) ? $_GET['facility'] : '';
$keyword = isset($_GET['keyword']) ? $_GET['keyword'] : '';
?>

<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>Confirm</title>
    <style>
        .center {
            margin-left: 100px;
            width: 300px;
            padding: 10px;
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        label {
            display: flex;
            width: 100%;
            padding: 5px;
        }

        label span {
            width: 30%;
            padding: 5px
        }

        label input, label select {
            width: 70%;
            border: 2px solid royalblue;
            padding-block: 6px;
            background-color: rgba(166, 183, 210, 0.81);
        }

        .primary_button, input[type="submit"] {
            border: 2px solid steelblue;
            background: cornflowerblue;
            border-radius: 10px;
            margin: auto;
            padding-block: 8px;
            padding-inline: 25px;
            color: white;
        }

        td, th {
            text-align: left;
            padding-right: 30px;

        }

        th:nth-child(4), td:nth-child(4) {
            width: 30%;
        }

        th:nth-child(5), td:nth-child(5) {
            padding-right: 0;
            text-align: center;
        }

        table {
            width: 100%;
        }

        .second_bt {
            background-color: #7493ad;
            border: 2px solid steelblue;
            padding-inline: 10px;
            padding-block: 3px;
            margin-block: 5px;
            color: white;
        }

        .popup {
            display: none;
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            padding: 20px;
            background-color: #fff;
            border: 1px solid #ccc;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2);
            z-index: 999;
        }

        .overlay {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0, 0, 0, 0.5);
            z-index: 998;
        }
    </style>
</head>
<body>

<div id="popup" class="popup">
    <p id="popup_alert">Bạn muốn xóa sinh viên này?</p>
    <button id="popup_delbtn">Xóa</button>
    <button onclick="close_popup()">Hủy</button>
</div>

<div id="overlay" class="overlay"></div>

<form class="center" method="GET" id="searchForm">
    <label>
        <span>Khoa</span>
        <select style="width: 75%" name="facility" id="facilitySelect">
            <?php
            $facility = [
                '' => '',
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu',
            ];
            foreach ($facility as $key => $value) {
                $selected = ($key == $selectedFacility) ? 'selected' : '';
                echo '<option value="' . $key . '" ' . $selected . '> ' . $value . '</option>';
            }
            ?>
        </select>
    </label>
    <label>
        <span>Từ khóa</span>
        <input type="text" name="keyword" value="<?php echo $keyword; ?>" id="keywordInput">
    </label>
    <label>
        <span></span>
        <input style="width: 40%" type="submit" value="Reset" onclick="resetForm(); return false;">
    </label>
</form>

<div>Số sinh viên tìm thấy: <span id="total">0</span></div>
<input style='float: right; margin-right: 4%' class='primary_button' type='button' value='Thêm' onclick="redirectToRegister()"> <br><br>

<div id="searchResults"></div>

<script>
    document.getElementById('searchForm').addEventListener('submit', function (event) {
        event.preventDefault(); // Prevent the form from submitting normally
        performSearch();
    });

    function updateTotal(value) {
        document.getElementById('total').textContent = value;
    }

    function redirectToRegister() {
        window.location.href = "register.php";
    }

    function resetForm() {
        document.getElementById('searchForm').reset();
        document.getElementById('searchResults').innerHTML = '';
    }

    document.getElementById('keywordInput').addEventListener('input', function() {
        performSearch();
    });

    document.getElementById('facilitySelect').addEventListener('change', function() {
        performSearch();
    });

    function performSearch() {
        var facility = document.getElementById('facilitySelect').value;
        var keyword = document.getElementById('keywordInput').value;

        // Perform AJAX request using fetch API
        fetch("student_search.php?facility=" + facility + "&keyword=" + keyword)
            .then(response => response.text())
            .then(data => {
                document.getElementById('searchResults').innerHTML = data;
            })
            .catch(error => console.error('Error:', error));
    }

    function popup(id, mode, mess="") {
        if (mode === 1) {
            document.getElementById('popup').style.display = 'block';
            document.getElementById('popup_alert').innerText = "Bạn muốn xóa sinh viên này (" + id + ")";
            document.getElementById('overlay').style.display = 'block';
            document.getElementById('popup_delbtn').addEventListener('click', function() {
                del_student(id);
            });
        }
    }

    function close_popup() {
        document.getElementById('popup').style.display = 'none';
        document.getElementById('overlay').style.display = 'none';
    }

    function del_student(id) {
        // Perform AJAX request using fetch API
        fetch("delete_student.php?id=" + id)
            .catch(error => console.error('Error:', error));
        close_popup();
        performSearch();
    }

    function update_student(id) {
        fetch("get_student.php?id=" + id)
            .then(response => response.json())
            .then(data => {
                var full_name = data['full_name'];
                var gender = data['gender'];
                var faculity = data['faculity'];
                var date_of_birth = data['date_of_birth'];
                var address = data['address'];

                // Tạo form động
                var form = document.createElement("form");
                form.setAttribute("method", "post");
                form.setAttribute("action", "update_student.php");

                // Tạo input fields ẩn và gán giá trị
                var idInput = document.createElement("input");
                idInput.setAttribute("type", "hidden");
                idInput.setAttribute("name", "student_id");
                idInput.setAttribute("value", id);
                form.appendChild(idInput);

                var fullNameInput = document.createElement("input");
                fullNameInput.setAttribute("type", "hidden");
                fullNameInput.setAttribute("name", "full_name");
                fullNameInput.setAttribute("value", full_name);
                form.appendChild(fullNameInput);

                var genderInput = document.createElement("input");
                genderInput.setAttribute("type", "hidden");
                genderInput.setAttribute("name", "gender");
                genderInput.setAttribute("value", gender);
                form.appendChild(genderInput);

                var faculityInput = document.createElement("input");
                faculityInput.setAttribute("type", "hidden");
                faculityInput.setAttribute("name", "faculity");
                faculityInput.setAttribute("value", faculity);
                form.appendChild(faculityInput);

                var dateOfBirthInput = document.createElement("input");
                dateOfBirthInput.setAttribute("type", "hidden");
                dateOfBirthInput.setAttribute("name", "date_of_birth");
                dateOfBirthInput.setAttribute("value", date_of_birth);
                form.appendChild(dateOfBirthInput);

                var addressInput = document.createElement("input");
                addressInput.setAttribute("type", "hidden");
                addressInput.setAttribute("name", "address");
                addressInput.setAttribute("value", address);
                form.appendChild(addressInput);

                document.body.appendChild(form);
                form.submit();
            })
            .catch(error => console.error('Error:', error));
    }


</script>
</body>
</html>
