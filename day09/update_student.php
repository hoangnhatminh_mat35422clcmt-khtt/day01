<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <title>update_student</title>
    <style>
        label {
            color: white;
            background-color: forestgreen;
            border: 2px solid royalblue;
            display: inline-block;
            margin-block: 8px;
            padding-block: 4px;
            text-indent: 20px;
            width: 110px;
        }

        .required::after{
            content: " *";
            color: red;
            font-size: 14px;
        }

        input[type="radio"] {
            color: forestgreen;
        }

        input[type='text'], input[type='date'], select{
            border: 2px solid royalblue;
            padding-block: 6px;
            margin-left: 15px;
        }

        input[type='submit'] {
            border: 2px solid royalblue;
            background: forestgreen;
            border-radius: 5px;
            margin-top: 20px;
            margin-bottom: 20px;
            padding: 10px;
            color: white;
            font-size: 15px;
        }

        input[type="date"] {
            color: #727272;
        }

        input[type="file"] {
            margin-left: 15px;
        }

        textarea {
            vertical-align: top;
            border: 2px solid royalblue;
            padding: 4px;
            display: inline-block;
            resize: none;
            padding-block: 6px;
            margin-block: 6px;
            margin-left: 15px;
        }

    </style>
</head>
<body>
<div style="border: solid 2px cornflowerblue; font-family: 'Times New Roman',serif; padding: 5%">
    <form method="POST" action="update_student_helper.php">
        <input type="hidden" name="student_id" value="<?php echo $_POST['student_id'];?>">
        <label class="required" for="full_name">Họ và tên</label>
        <input id="full_name" type="text" name="full_name" style="width: calc(100% - 150px)" value="<?= isset($_POST['full_name']) ? $_POST['full_name'] : '' ?>"> <br>

        <label class="required" for="gender">Giới tính</label>
        <?php
        $gender = [
            0 => 'Nam',
            1 => 'Nữ',
        ];
        for ($i = 0; $i < 2; $i++) {
            $checked = isset($_POST['gender']) && $_POST['gender'] == $gender[$i] ? 'checked' : '';
            echo '<input style="margin-left: 15px" type="radio" name="gender" id="radio_'. $i .'" value="' . $gender[$i] . '" ' . $checked . '> ' . $gender[$i];
        }
        ?> <br>

        <label class="required">Phân khoa</label>
        <select id="select" name="faculity" style="width: 40%">
            <?php
            $facility = [
                '' => '--Chọn phân khoa--',
                'MAT' => 'Khoa học máy tính',
                'KDL' => 'Khoa học vật liệu',
            ];
            foreach ($facility as $key => $value) {
                $selected = isset($_POST['faculity']) && $_POST['faculity'] == $value ? 'selected' : '';
                echo '<option value="' . $value . '" ' . $selected . '> ' . $value . '</option>';
            }
            ?>
        </select> <br>

        <label class="required">Ngày sinh</label>
        <input id="birthday" type="text" name="date_of_birth" placeholder="dd/mm/yyyy" style="width: calc(40% - 5px)" value="<?= isset($_POST['date_of_birth']) ? $_POST['date_of_birth'] : '' ?>"><br>

        <label>Địa chỉ</label>
        <textarea id="address" name="address" rows="5" style="width: calc(100% - 150px)"><?= isset($_POST['address']) ? $_POST['address'] : '' ?></textarea>

        <label for="fileSelect">Hình ảnh</label>
        <input type="file" name="image" id="fileSelect"><br>

        <div style="display:flex; justify-content: center;">
            <input type="submit" value="Xác nhận">
        </div>
    </form>
</div>
<script>

    $('input[type="submit"]').click(function (event){
        event.preventDefault();

        var f = $(this).closest('form');
        f.submit();
    });
</script>
</body>
</html>
