
<?php
include 'db_connect.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $id = $_POST['student_id'];
    $full_name = $_POST['full_name'];
    $gender = $_POST['gender'];
    $faculity = $_POST['faculity'];
    $date_of_birth = $_POST['date_of_birth'];
    $address = $_POST['address'];

    // Sử dụng prepared statement để cập nhật thông tin nhân viên
    $stmt = $conn->prepare("UPDATE students SET full_name = ?, gender = ?, faculity = ?, date_of_birth = ?, address = ? WHERE id = ?");
    $stmt->bind_param("sssssi", $full_name, $gender, $faculity, $date_of_birth, $address, $id);
    $stmt->execute();
    $stmt->close();

    echo "<script> window.location.href = 'students.php?'; ";
    echo "</script>";
}

?>
