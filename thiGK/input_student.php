<!DOCTYPE html>
<html>
<head>
    <title>Đăng ký sinh viên</title>
    <style>
        .column {
            float: left;
            width: 33.33%;
            padding: 10px;
            box-sizing: border-box;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
<form action="regist_student.php" method="POST"  id="my_form">
    <table>
        <tr>
            <td>Họ và tên</td>
            <td><input type="text" name="ho_ten" id="ho_ten"></td>
        </tr>
        <tr>
            <td>Giới tính</td>
            <td>
                <input type="radio" name="gioi_tinh" value="1" id="gioi_tinh1">Nam
                <input type="radio" name="gioi_tinh" value="2" id="gioi_tinh2">Nữ
            </td>
        </tr>
        <tr>
            <td>Ngày sinh</td>
            <td>
                <div class="column">
                    Năm <select name="nam" id="nam">
                        <!-- Thêm các option năm từ {năm hiện tại - 40} đến {năm hiện tại - 15} -->
                        <option></option>
                    </select>
                </div>
                <div class="column">
                    Tháng <select name="thang" id="thang">
                        <option></option>
                        <!-- Thêm các option từ 1 đến 12 -->
                    </select>
                </div>
                <div class="column">
                    Ngày <select name="ngay" id="ngay">
                        <option></option>
                        <!-- Thêm các option từ 1 đến 31 -->
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td>Địa chỉ</td>
            <td>
                Thành phố <select name="thanh_pho" id="thanh_pho" onchange="getQuan()">
                    <option></option>
                    <option value="Hà Nội">Hà Nội</option>
                    <option value="Tp.Hồ Chí Minh">TP. Hồ Chí Minh</option>
                    <!-- Thêm các option của các thành phố khác -->
                </select>
                Quận <select name="quan" id="quan">
                    <option></option>
                    <!-- Thêm các option của các quận khác -->
                </select>
            </td>
        </tr>
        <tr>
            <td>Thông tin khác</td>
            <td><textarea name="thong_tin_khac" rows="4" cols="50"></textarea></td>
        </tr>
    </table>
    <input type="submit" value="Đăng ký">
</form>

<div id="error" style="color: red;"></div>

<script>
    var select = document.getElementById("nam");
    var currentYear = 2023;
    for (var i = currentYear - 40; i <= currentYear - 15; i++) {
        var option = document.createElement("option");
        option.text = i;
        option.value = i;
        select.appendChild(option);
    }

    var selectThang = document.getElementById("thang");
    for (var i = 1; i <= 12; i++) {
        var option1 = document.createElement("option");
        if (i < 10) {
            option1.text = '0' + i;
            option1.value = '0' + i;
        } else {
            option1.text = i;
            option1.value = i;
        }
        selectThang.appendChild(option1);
    }

    var selectNgay = document.getElementById("ngay");
    for (var i = 1; i <= 31; i++) {
        var option2 = document.createElement("option");
        if (i < 10) {
            option2.text = '0' + i;
            option2.value = '0' + i;
        } else {
            option2.text = i;
            option2.value = i;
        }
        selectNgay.appendChild(option2);
    }

    function getQuan() {
        var selectCity = document.getElementById("thanh_pho");
        var selectDistrict = document.getElementById("quan");
        selectDistrict.innerHTML = "";
        if (selectCity.value === "Hà Nội") {
            var districts = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];
            for (var i = 0; i < districts.length; i++) {
                var option = document.createElement("option");
                option.text = districts[i];
                option.value = districts[i];
                selectDistrict.appendChild(option);
            }
        } else if (selectCity.value === "Tp.Hồ Chí Minh") {
            var districts = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
            for (var i = 0; i < districts.length; i++) {
                var option = document.createElement("option");
                option.text = districts[i];
                option.value = districts[i];
                selectDistrict.appendChild(option);
            }
        }
    }

    function checkForm(event) {
        event.preventDefault();
        var errorDiv = $("#error");
        var hoTen = $("#ho_ten").val();
        if (hoTen === "") {
            errorDiv.text("Hãy nhập họ tên.");
            return false;
        }

        var gioiTinh1 = $("#gioi_tinh1").is(":checked");
        var gioiTinh2 = $("#gioi_tinh2").is(":checked");
        if (!gioiTinh1 && !gioiTinh2) {
            errorDiv.text("Hãy chọn giới tính.");
            return false;
        }

        var ngay = $("#ngay").val();
        var thang = $("#thang").val();
        var nam = $("#nam").val();
        if (ngay === "" || thang === "" || nam === "") {
            errorDiv.text("Hãy chọn ngày sinh.");
            return false;
        }

        var thanhpho = $('#thanh_pho').val();
        var quan = $('#quan').val();
        if (thanhpho === "" || quan === "") {
            errorDiv.text("Hãy chọn địa chỉ.")
            return false
        }
        return true;
    }


    $('input[type="submit"]').click(function (event){
        if (checkForm(event) === true) {
            $(this).closest('form').submit();
        }
    });

</script>

</body>
</html>
