<!DOCTYPE html>
<html>
<head>
    <title>Thông tin sinh viên</title>
</head>
<body>
<table>
    <tr>
        <td>Họ và tên</td>
        <td><?php echo isset($_POST['ho_ten']) ? $_POST['ho_ten'] : ''; ?></td>
    </tr>
    <tr>
        <td>Giới tính</td>
        <td><?php echo isset($_POST['gioi_tinh']) ? ($_POST['gioi_tinh'] == 1 ? 'Nam' : 'Nữ') : ''; ?></td>
    </tr>
    <tr>
        <td>Ngày sinh</td>
        <td><?php echo isset($_POST['ngay']) && isset($_POST['thang']) && isset($_POST['nam']) ? $_POST['ngay'] . '/' . $_POST['thang'] . '/' . $_POST['nam'] : ''; ?></td>
    </tr>
    <tr>
        <td>Địa chỉ</td>
        <td><?php echo isset($_POST['quan']) && isset($_POST['thanh_pho']) ? $_POST['quan'] . ' - ' . $_POST['thanh_pho'] : ''; ?></td>
    </tr>
    <tr>
        <td>Thông tin khác</td>
        <td><?php echo isset($_POST['thong_tin_khac'])? $_POST['thanh_pho'] : ''; ?></td>
    </tr>
</table>
</body>
</html>
